# Jekyll and Gitlab

This repository hosts example assets for a Jekyll website, and a talk for EdLUG written in *hovercraft!*.

## Presentation

Run the following to ensure you have hovercraft ability

    sudo apt-get install -y python3 python3-pip

Run the following to start a presentation

    bin/hover.sh talk/main.rst

## Jekyll Demo

There is a script under `jekyll-scripts/` to set up your Ubuntu environment for ruby and Jekyll:

    cd jekyll-scripts
    ./setup.sh
    . ~/.bashrc

If you have never set up ruby before, run this step; **otherwise skip it** as it assumes some preferences for you upon setup.

You can then run the `transcript.sh` script to run the demo, or step through it manually.
