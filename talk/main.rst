:title: Sites with Jekyll
:css: styles.css

----

Sites with Jekyll and Gitlab
============================

EdLUG June 2019
---------------

Tai Kedzierski

----

New Website
===========

Let's have a quick look

.. note::

    <https://edlug.gitlab.io>

    or, `git checkout master && ./build.sh && firefox http://localhost:4000`

    We have sections, mailing list page, a Getting Started area

----

Why
===

* Why not keep what we had?
* Why Jekyll?
* Why Gitlab?

----

Problems
========

* Drupal needs maintenance
* SSL Cert management
* Redundant features

.. note::

    * Drupal is a PHP application running in a hosted service
    * CVEs regularly published
    * Accounts and passwords to manage
    * HTTPS not easy to control on hosted services
        * Could use a separate DNS server we have control of
        * Extra infrastructure
    * Redundant sections and added features
    * Shared hosting exposes us to other sites' vulnerabilities
    * Not my favorite platform

----

Answer part 1 : Jekyll
----------------------

* Static Sites are just HTML, CSS and JavaScript
* No back-end, no Server-Side Scripting
* Host anywhere

.. note::

    * Jekyll sites aren't vulnerable at application level
    * Webserver is the only potential target
    * We can move a static site to any hosting provider
        * Not dependent on provider's features

----

Answer part 2 : Gitlab Pages
----------------------------

* Nobody can use server-side scripting
    * :code:`.htaccess` :-(
* Anybody can participate
    * With gatekeeping
* Gitlab.com takes care of account management and permissions

.. note::

    * Nobody can run server-side scripting
    * Generic hosts assume people want to use SSS
    * .htaccess can mess things up
    * Review process allows anybody to send in changes as they designed them
        * There could be a section for members' own sites and blogs
        * That can curated and amended by anybody
    * Anybody can write stuff in
        * If something escapes review, the only risk is broken layout
        * History of changes kept
    * Gitlab.com has its own accounts system which controls who is gatekeeper, and who is contributor

----

Answer part 3 : Outsource functionality
---------------------------------------

* Calendar is Open Tech Calendar
* HTTPS is managed by Gitlab
* Mailing list is still with Lug.org.uk

.. note::

    * We had a calendar section which we referenced in the old site
    * HTTPS does not need to be a separate piece of infrastructure
        * At least, for the gitlab subdomain
    * Mailing list was actually not self-hosted

----

Collaboration Example
=====================

Code of Conduct
---------------

.. note::

    * I received 1 diff patch
    * Comments on GitLab
        * These could have been pull requests
    * Some pushback on the list
    * All resolved as far as participation is involved.

----


Jekyll Demo
===========

Let's see about creating a Jekyll website

----

Gitlab CI Demo
==============

.. note::

    * Uses docker containers to provide a clean build environment
    * Can build anything so long as you state where to get the tools
        * or provide the tools somehow
