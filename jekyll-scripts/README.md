# Jekyll scripts

Two scripts, and a GitLab CI configuration

* `setup.sh` to ensure your local environment has jekyll
* `serve.sh` wrapper to serve your site on `http://localhost:4000`
* `.gitlab-ci.yml` To make GitLab build your site on its infrastructure
* `transcript.sh` run this to automatically run all the commands background, whilst discussing them
    * To run it without pausing, set `export NO_PAUSE=true` in your environment
