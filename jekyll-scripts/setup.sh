sudo apt-get update && sudo apt-get install ruby-full build-essential zlib1g-dev ruby-bundler -y

echo -e 'export GEM_HOME="$HOME/gems"\nexport PATH="$HOME/gems/bin:$PATH"' >> ~/.bashrc
. ~/.bashrc

gem install jekyll
bundle install

echo "You may want to re-source your .bashrc file now."
