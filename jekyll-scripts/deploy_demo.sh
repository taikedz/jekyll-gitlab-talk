#!/usr/bin/env bash

set -euo pipefail

#===============
# pre-talk shim
# Deploy a dummy site
if [[ "${DUMMY:-}" = true ]]; then
    mkdir public
    echo "<h1>Placeholder</h1>" > public/index.html
    exit
fi
#===============

cd jekyll-scripts
GITLAB_DEMO=true NO_PAUSE=true bash transcript.sh
cd mysite
jekyll build -d ../../public
