#!/usr/bin/env bash

set -euo pipefail

# ============== Demo sections

demo_part1() {
        # ===============================
        # Part 1: create a basic jekyll site with the default theme "minima"

    jekyll new mysite
    cd mysite

        # Add some basic info
    set-config author "EdLUG Presenter"
    set-config title "Demo Jekyll site"

    gitlab_ci_demo_setup

        # Demo 1
    echo 'We can now run `jekyll serve -d public watch` to view the website'
    echo 'If we make edits to pages, they update automatically'
    show _config.yml
}

gitlab_ci_demo_setup() {
    # Gitlab CI demo deployment
    if [[ "${GITLAB_DEMO:-}" = true ]]; then
        set-config baseurl '"/jekyll-gitlab-talk"'
    fi
}

demo_part2() {

        # ============================================
        # Part 2 : Setting up Prologue theme

        # Add a theme from https://github.com/jameshamann/jekyll-material-theme
        # More themes in http://jekyllthemes.org
    echo 'gem "jekyll-theme-prologue"' >> Gemfile

        # Update environment Gemfile
    bundle

        # bundle command's default run introduces an incompatibility - not sure why
        # Delete after running `bundle`, during runtime it will be regenerated
        #   in a compatible format
    rm Gemfile.lock

        # Activate it in Jekyll config
    set-config theme "jekyll-theme-prologue"

    mkdir assets/images -p # Where we put various non-pre-processed files
    cp ../penguin.png assets/images/penguin.png

        ## Specific configuration for the Prologue theme
        # Documented at https://github.com/chrisbobbe/jekyll-theme-prologue#build-your-homepage

    set-config avatar "/assets/images/penguin.png"

    set-config collections "[sections]"
    mkdir _sections
    cat <<'EOF' >> _sections/home.html
---
title: Welcome Home
cover-photo: assets/images/cover.jpg
cover-photo-alt: Welcome
---
EOF
    cp ../cover.jpg assets/images/cover.jpg

    show _config.yml
    echo "We can now see the site with a new theme"
}

demo_part3() {
        # ==============================
        # Part 3: add a blog, fix the 404

        # If you want a blog, you must setup the section
    cat <<'EOF' > blog.html
---
title: Here is the blog
layout: blog
---
EOF

        # Prologue processes all *.html files in top directory
        # It should not produce a section for the "404.html" file, naturally
    add-header-before layout "404.html" hide "true"
        # Alternatively, we could have replaced the existing one with the theme's Gem
        # wget 'https://github.com/chrisbobbe/jekyll-theme-prologue/raw/master/404.html' -O 404.html

    show "404.html"
    show "blog.html"
    echo "Added the blog back, and fixed the 404 generating section"
}

# =========================
# Helpful functions

set-config() {
    local label="$1"; shift
    local value="$1"; shift

    if grep -qE "^$label:" _config.yml; then
        sed -r "s%^$label:.*%$label: $value%" -i _config.yml
    else
        echo "$label: $value" >> _config.yml
    fi
}

add-header-before() {
    local before_label="$1"; shift
    local file="$1"; shift
    local label="$1"; shift
    local value="$1"; shift

    sed -r "s%^$before_label:%$label: $value\n$before_label:%" -i "$file"
}

pause-continue() {
    if [[ "${NO_PAUSE:-}" != true ]]; then
        read -p "Press return to continue..."
    fi
}

network_check() {
    ping -c 2 www.example.com >/dev/null
}

show() {
    if [[ "${NO_PAUSE:-}" = true ]]; then
        return 0
    fi

    echo -e "\033[31;1m$1\033[0m"
    sed -r 's/^/>\t/' "$1"
}

trap onexit EXIT
onexit() {
    if [[ "$?" != 0 ]]; then
        echo -e "\033[31;1mFAILED\033[0m"
    fi
}

# ====================

main() {
    network_check

    demo_part1
    pause-continue

    demo_part2
    pause-continue

    demo_part3
    pause-continue

    echo "End."
}

main "$@"
