#!/usr/bin/env bash

wd="${1:-$PWD}"

cd "$wd"

echo "Running in '$PWD'"

jekyll serve -d public --watch
